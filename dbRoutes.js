const Subtitles = require('./models').Subtitles;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
        Subtitles.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })



    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }

        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = Subtitles.findOne(rest);
        } else {
            q = Subtitles.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((subtitles) => {
            return res.json(subtitles);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        Subtitles.create(data)
            .then((subtitle) => {
                return res.json(subtitle);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        Subtitles.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => Subtitles.find(conditions))
            .then(subtitles => {
                return res.json(subtitles);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let subtitles;
        Subtitles.find(conditions)
            .then((a) => {
                subtitles = a;
                return Subtitles.remove(conditions)
            })
            .then(() => {
                return res.json(subtitles);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        Subtitles.findById(req.params.id)
            .then((subtitle) => {
                return res.json(subtitle);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        Subtitles.findByIdAndUpdate(id, { $set: changes })
            .then(() => Subtitles.findById(id))
            .then(subtitle => {
                return res.json(subtitle);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedSubtitles;
        Subtitles.findById(id)
            .then(subtitle => {
                deletedSubtitles = subtitle;
                return Subtitles.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedSubtitles);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })


    return router;
}