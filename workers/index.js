module.exports = ({ rabbitmqChannel }) => {
  const exporterWorker = require("@videowiki/workers/exporter")({
    rabbitmqChannel,
  });
  return { exporterWorker };
};
