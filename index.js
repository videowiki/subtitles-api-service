const { server, app, createRouter } = require('./generateServer')();
const videowikiGenerators = require('@videowiki/generators');
const rabbitmqService = require('@videowiki/workers/vendors/rabbitmq');
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;

const mongoose = require('mongoose');
const DB_CONNECTION = process.env.SUBTITLES_SERVICE_DATABASE_URL;
let mongoConnection;
let rabbitmqChannel;
mongoose.connect(DB_CONNECTION)
.then(con => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        channel.on('error', (err) => {
            console.log('RABBITMQ ERROR', err)
            process.exit(1);
        })
        channel.on('close', () => {
            console.log('RABBITMQ CLOSE')
            process.exit(1);
        })
        
        videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection, rabbitmqConnection: rabbitmqChannel.connection });
        
        const workers = require('./workers')({ rabbitmqChannel })
        const controller = require('./controller')({ workers });

        app.use('/db', require('./dbRoutes')(createRouter()));

        app.all('*', (req, res, next) => {
            if (req.headers['vw-user-data']) {
                try {
                    const user = JSON.parse(req.headers['vw-user-data']);
                    req.user = user;
                } catch (e) {
                    console.log(e);
                }
            }
            next();
        })

        app.get('/by_article_id/:id', controller.getByArticleId);
        app.post('/:id/subtitles', controller.addSubtitle);

        // TODO: DOC THIS
        app.post('/:id/activated', controller.activateSubtitles);

        // TODO: DOC THIS
        app.post('/:id/subtitles/combine', controller.combineSubtitle);

        app.post('/:id/subtitles/:subtitlePosition/split', controller.splitSubtitle);
        app.patch('/:id/subtitles/:subtitlePosition', controller.updateSubtitle);
        app.delete('/:id/subtitles/:subtitlePosition', controller.deleteSubtitle);

        app.post('/:id/reset', controller.resetSubtitles);
        app.get('/:id', controller.getById);

        const PORT = process.env.PORT || 4000;
        server.listen(PORT)
        console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
        console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)

    })
})
.catch(err => {
    console.log('Mongodb connection error', err);
    process.exit(1);
})