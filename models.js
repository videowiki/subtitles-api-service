const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SPEAKER_GENDER_ENUM = ['male', 'female'];

const SpeakerProfileSchema = new Schema({
    speakerGender: { type: String, enum: SPEAKER_GENDER_ENUM },
    speakerNumber: { type: Number }, // To be Speaker 1, Speaker 2, Speaker 3...etc
})

const SubtitleSchema = new Schema({

    startTime: { type: Number },
    endTime: { type: Number },
    
    position: { type: Number },
    slidePosition: { type: Number },
    subslidePosition: { type: Number },
    
    speakerProfile: SpeakerProfileSchema,
    
    text: { type: String },
    
});

const SubtitlesSchema = new Schema({
    article: { type: Schema.Types.ObjectId, index: true },
    video: { type: Schema.Types.ObjectId },
    organization: { type: Schema.Types.ObjectId },

    subtitles: [SubtitleSchema],
    activated: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: { type: Date, default: Date.now },
})

const Subtitles = mongoose.model('subtitles', SubtitlesSchema);

module.exports = { Subtitles };